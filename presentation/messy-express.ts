const express = require('express');
const app = express();
const mysql = require('mysql');

// Database connection configuration
const db = mysql.createConnection({
  host: 'localhost',
  user: 'yourusername',
  password: 'yourpassword',
  database: 'mydb'
});

db.connect((err) => {
  if (err) throw err;
  console.log('Connected to the database');
});

// Business logic for fetching users from the database
app.get('/api/users', (req, res) => {
  db.query('SELECT * FROM users', (err, result) => {
    if (err) throw err;
    res.json(result);
  });
});

// Business logic for creating a new user
app.post('/api/users', (req, res) => {
  const { username, email } = req.body;
  if (username && email) {
    const newUser = { username, email };
    db.query('INSERT INTO users SET ?', newUser, (err, result) => {
      if (err) throw err;
      res.status(201).json({ message: 'User created successfully' });
    });
  } else {
    res.status(400).json({ message: 'Invalid input' });
  }
});

// More routes and added business logic all in the same file...
