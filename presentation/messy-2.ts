app.put('/api/users/:id', async (req, res) => {
  const userId = req.params.id;
  const { username, email, password } = req.body;

  try {
    // Update user info in 'user' table
    const updateUserQuery = `UPDATE users SET username = '${username}', email = '${email}' WHERE id = ${userId}`;
    await runQuery(updateUserQuery);

    // Update user password in 'password' table
    const updatePasswordQuery = `UPDATE password SET password = '${password}' WHERE user_id = ${userId}`;
    await runQuery(updatePasswordQuery);

    res.status(200).json({ message: 'User info and password updated successfully' });
  } catch (error) {
    res.status(500).json({ message: 'An error occurred while updating user info and password' });
  }
});
