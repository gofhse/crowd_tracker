CREATE TABLE "booking" (
  "id" integer PRIMARY KEY NOT NULL,
  "created_at" timestamp NOT NULL,
  "updated_at" timestamp NOT NULL,
  "coworking_id" integer NOT NULL,
  "coworking_space_id" integer NOT NULL,
  "start_time" timestamp NOT NULL,
  "end_time" timestamp NOT NULL,
  "user_id" integer NOT NULL
);
