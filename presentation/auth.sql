CREATE TABLE "password" (
  "id" integer PRIMARY KEY NOT NULL,
  "user_id" integer NOT NULL,
  "password_hash" varchar NOT NULL
);

CREATE TABLE "user" (
  "id" integer PRIMARY KEY NOT NULL,
  "created_at" timestamp NOT NULL,
  "updated_at" timestamp,
  "login" varchar NOT NULL,
  "email" varchar,
  "is_active" bool NOT NULL,
  "phone_number" varchar,
  "first_name" varchar,
  "last_name" varchar,
  "date_of_birth" timestamp
);

ALTER TABLE "password" ADD FOREIGN KEY ("user_id") REFERENCES "user" ("id");
