export class MetricsCalculator {
  static calculateWeightedMethodsPerClass(targetClass: any): number {
    const methods = Object.getOwnPropertyNames(targetClass.prototype);

    let weightedMethods = 0;

    methods.forEach((method) => {
      const descriptor = Object.getOwnPropertyDescriptor(targetClass.prototype, method);

      if (descriptor && typeof descriptor.value === 'function') {
        weightedMethods += 1;
      }
    });

    return weightedMethods;
  }

  static calculateCouplingBetweenClasses(class1: any, class2: any): number {
    // Example: Calculate coupling based on shared method names
    const methodsClass1 = new Set(Object.getOwnPropertyNames(class1.prototype));
    const methodsClass2 = new Set(Object.getOwnPropertyNames(class2.prototype));

    const sharedMethods = Array.from(new Set([...methodsClass1].filter(x => methodsClass2.has(x))));

    return sharedMethods.length;
  }

  static calculateNumberOfChildren(parentClass: any): number {
    const childClasses = Object.values(parentClass).filter((child) => typeof child === 'function');

    return childClasses.length;
  }

  static calculateDepthOfInheritanceTree(targetClass: any, depth = 0): number {
    const parentClass = Object.getPrototypeOf(targetClass);

    if (parentClass === Object.prototype || parentClass === null) {
      return depth;
    }

    return this.calculateDepthOfInheritanceTree(parentClass, depth + 1);
  }
}
