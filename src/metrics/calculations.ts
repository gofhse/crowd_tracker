import { AuthService } from "../apps/auth/src/auth.service";
import { BookingsService } from "../apps/bookings/src/bookings.service";
import { MetricsCalculator } from "./metrics";
import { UsersService } from "../apps/auth/src/users/users.service";

// const metrics1 = MetricsCalculator.calculateWeightedMethodsPerClass(BookingsService);
// console.log(`Weighted Methods for BookingsService: ${metrics1}`);
console.log('-------------------BookingsService-------------------');
const weightedMethods1 = MetricsCalculator.calculateWeightedMethodsPerClass(BookingsService);
console.log(`Weighted Methods for BookingsService: ${weightedMethods1}`);

const coupling1 = MetricsCalculator.calculateCouplingBetweenClasses(BookingsService, AuthService);
console.log(`Coupling between BookingsService and AuthService: ${coupling1}`);
const coupling2 = MetricsCalculator.calculateCouplingBetweenClasses(BookingsService, UsersService);
console.log(`Coupling between BookingsService and UsersService: ${coupling2}`);
console.log('BookingsService Coupling: ', coupling1 + coupling2);

const numberOfChildren1 = MetricsCalculator.calculateNumberOfChildren(BookingsService);
console.log(`Number of Children for BookingsService: ${numberOfChildren1}`);

const depthOfInheritanceTree1 = MetricsCalculator.calculateDepthOfInheritanceTree(BookingsService);
console.log(`Depth of Inheritance Tree for BookingsService: ${depthOfInheritanceTree1}`);


console.log('-------------------AuthService-------------------');
const weightedMethods2 = MetricsCalculator.calculateWeightedMethodsPerClass(AuthService);
console.log(`Weighted Methods for AuthService: ${weightedMethods2}`);

const coupling3 = MetricsCalculator.calculateCouplingBetweenClasses(AuthService, UsersService);
console.log(`Coupling between AuthService and UsersService: ${coupling3}`);
const coupling4 = MetricsCalculator.calculateCouplingBetweenClasses(AuthService, BookingsService);
console.log(`Coupling between AuthService and BookingsService: ${coupling4}`);
console.log('AuthService Coupling: ', coupling3 + coupling4);

const numberOfChildren2 = MetricsCalculator.calculateNumberOfChildren(AuthService);
console.log(`Number of Children for AuthService: ${numberOfChildren2}`);

const depthOfInheritanceTree2 = MetricsCalculator.calculateDepthOfInheritanceTree(AuthService);
console.log(`Depth of Inheritance Tree for AuthService: ${depthOfInheritanceTree2}`);


console.log('-------------------UsersService-------------------');
const weightedMethods3 = MetricsCalculator.calculateWeightedMethodsPerClass(UsersService);
console.log(`Weighted Methods for UsersService: ${weightedMethods3}`);

const coupling5 = MetricsCalculator.calculateCouplingBetweenClasses(UsersService, AuthService);
console.log(`Coupling between UsersService and AuthService: ${coupling5}`);
const coupling6 = MetricsCalculator.calculateCouplingBetweenClasses(UsersService, BookingsService);
console.log(`Coupling between UsersService and BookingsService: ${coupling6}`);
console.log('UsersService Coupling: ', coupling5 + coupling6);

const numberOfChildren3 = MetricsCalculator.calculateNumberOfChildren(UsersService);
console.log(`Number of Children for UsersService: ${numberOfChildren3}`);

const depthOfInheritanceTree3 = MetricsCalculator.calculateDepthOfInheritanceTree(UsersService);
console.log(`Depth of Inheritance Tree for UsersService: ${depthOfInheritanceTree3}`);