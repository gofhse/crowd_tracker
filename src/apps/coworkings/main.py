import os

import aiopg
from aiohttp import web

app = web.Application()
routes = web.RouteTableDef()

dsn = os.getenv("dns")


async def get_db_pool():
    return await aiopg.create_pool(dsn)


async def execute_query(query, params=None):
    async with get_db_pool() as pool:
        async with pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute(query, params)
                return await cur.fetchall()


@routes.get("/search")
async def search_coworkings(request):
    location = request.query.get("location")
    amenities = request.query.getall("amenities")
    query = "SELECT * FROM coworking WHERE location = %s AND amenities = %s"
    results = await execute_query(query, (location, amenities))
    coworking_spaces = [dict(row) for row in results]
    return web.json_response({"coworking_spaces": coworking_spaces})


@routes.get("/coworkings")
async def get_coworkings(request):
    filter_base_string = request.query.get("filter-base-string")
    page_id = request.query.get("page-id")
    page_size = request.query.get("page-size")
    query = "SELECT * FROM coworking WHERE filter_base_string = %s LIMIT %s OFFSET %s"
    results = await execute_query(query, (filter_base_string, page_size, page_id))
    coworking_list = [dict(row) for row in results]
    return web.json_response({"coworking_list": coworking_list})


@routes.get("/coworkings/{coworking_id}")
async def get_coworking_by_id(request):
    coworking_id = request.match_info["coworking_id"]
    query = "SELECT * FROM coworking WHERE id = %s"
    results = await execute_query(query, (coworking_id,))
    coworking = [dict(row) for row in results]
    return web.json_response({"coworking": coworking})


@routes.get("/coworkings/{coworking_id}/spaces")
async def get_coworking_spaces(request):
    coworking_id = request.match_info["coworking_id"]
    month = request.query.get("month")
    year = request.query.get("year")

    query = """
        SELECT * FROM coworkingSpace
        WHERE coworking_id = %s
        AND month = %s
        AND year = %s
    """
    results = await execute_query(query, (coworking_id, month, year))
    spaces_and_costs = [dict(row) for row in results]
    return web.json_response({"spaces_and_costs": spaces_and_costs})


@routes.get("/coworkings/{coworking_id}/prediction")
async def get_crowd_prediction(request):
    coworking_id = request.match_info["coworking_id"]
    prediction_date = request.query.get("date")

    query = """
        SELECT prediction_result
        FROM prediction_table
        WHERE coworking_id = %s AND prediction_date = %s
    """
    results = await execute_query(query, (coworking_id, prediction_date))

    if results:
        prediction_result = results[0][0]
        return web.json_response({"prediction": prediction_result})
    else:
        return web.json_response(
            {"prediction": "No prediction available for the given date"}
        )


@routes.put("/coworkings/{coworking_id}/spaces/{space_id}")
async def update_coworking_spaces(request):
    coworking_id = request.match_info["coworking_id"]
    space_id = request.match_info["space_id"]

    try:
        space = await request.json()
    except Exception:
        return web.json_response({"error": "Invalid JSON format"}, status=400)
    query = """
        UPDATE coworkingSpace
        SET seat_number = %s, is_available = %s, cost = %s
        WHERE coworking_id = %s AND id = %s
    """

    seat_number = space.get("seat_number")
    is_available = space.get("is_available")
    cost = space.get("cost")
    await execute_query(
        query, (seat_number, is_available, cost, coworking_id, space_id)
    )
    return web.json_response({"message": "Spaces updated successfully"})


@routes.post("/coworkings/{coworking_id}/images")
async def update_coworking_images(request):
    coworking_id = request.match_info["coworking_id"]
    try:
        data = await request.json()
        path = data.get("path")
    except Exception:
        return web.json_response({"error": "Invalid JSON format"}, status=400)
    add_image_query = (
        "INSERT INTO coworkingImage (coworking_id, image_path) VALUES (%s, %s)"
    )
    await execute_query(add_image_query, (coworking_id, path))
    return web.json_response({"message": "Coworking image created successfully"})


@routes.get("/coworkings/{coworking_id}/images")
async def get_coworking_images(request):
    coworking_id = request.match_info["coworking_id"]
    get_image_query = "SELECT * from coworkingImage WHERE coworking_id = %s"
    results = await execute_query(get_image_query, (coworking_id,))
    images = [dict(row) for row in results]
    return web.json_response({"images": images})


@routes.delete("/coworkings/{coworking_id}/images/{image_id}")
async def del_coworking_images(request):
    coworking_id = request.match_info["coworking_id"]
    image_id = request.match_info["image_id"]
    remove_image_query = (
        "DELETE FROM coworkingImage WHERE coworking_id = %s AND image_id = %s"
    )
    await execute_query(remove_image_query, (coworking_id, image_id))
    return web.json_response({"message": "Coworking image deleted successfully"})


app.router.add_routes(routes)

if __name__ == "__main__":
    web.run_app(app)
