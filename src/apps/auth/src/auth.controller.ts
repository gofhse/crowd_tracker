import { Body, Controller, Get, Param, Post, Res, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UsersService } from './users/users.service';
import { CreateUserDto, UpdatePasswordDto } from './users/dto';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { CurrentUser } from './decorators/current-user.decorator';
import { UserEntity } from './users/entities/user.entity';
import { Response } from 'express';
import JwtAuthGuard from './guards/jwt-auth.guard';
import { MessagePattern } from '@nestjs/microservices';

@Controller()
export class AuthController {
  constructor(private readonly authService: AuthService, private readonly usersService: UsersService) {}

  @Post('/register')
  async register(@Body() createUserDto: CreateUserDto): Promise<void> {
    await this.usersService.createUser(createUserDto);
  }

  @Get('/profile:userId')
  async getProfile(@Param('userId') userId: number): Promise<any> {
    return await this.usersService.getUser(userId);
  }

  @Post('forgot-password')
  async forgotPassword(@Body() userId: number, @Body() login: string): Promise<any> {
    return await this.usersService.resetPassword(userId, login);
  }

  @Post('change-password/:userId')
  async changePassword(@Param('userId') userId: number, @Body() currentPassword: string, @Body() updatePasswordDto: UpdatePasswordDto): Promise<any> {
    return await this.usersService.updatePassword(currentPassword, updatePasswordDto);
  }

  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(
    @CurrentUser() user: UserEntity,
    @Res({ passthrough: true }) response: Response,
  ) {
    await this.authService.login(user, response);
    response.send(user);
  }

  @UseGuards(JwtAuthGuard)
  @MessagePattern('validate_user')
  async validateUser(@CurrentUser() user: UserEntity) {
    return user;
  }
}
