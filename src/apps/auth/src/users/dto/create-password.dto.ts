import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreatePasswordDto {
  @IsNotEmpty()
  @IsNumber()
  userId: number;

  @IsNotEmpty()
  @IsString()
  passwordHash: string;
}