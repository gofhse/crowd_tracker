import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class UpdateUserDto {
  @IsNumber()
  updatedAt?: Date;

  @IsString()
  login?: string;

  @IsString()
  email?: string;

  @IsString()
  phoneNumber?: string;

  @IsString()
  firstName?: string;

  @IsString()
  lastName?: string;

  @IsNumber()
  dateOfBirth?: Date;

  // @IsString()
  // password?: string;
}