import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  @IsString()
  login: string;

  @IsString()
  email?: string;

  @IsString()
  phoneNumber?: string;

  @IsString()
  firstName?: string;

  @IsString()
  lastName?: string;

  @IsNumber()
  dateOfBirth?: Date;

  @IsNotEmpty()
  @IsString()
  password: string;
}