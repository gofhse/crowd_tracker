import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class UpdatePasswordDto {
  @IsNotEmpty()
  @IsNumber()
  userId: number;

  @IsNotEmpty()
  @IsString()
  password: string;
}