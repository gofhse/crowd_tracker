import { Injectable, NotFoundException, UnauthorizedException, UnprocessableEntityException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto, UpdateUserDto, CreatePasswordDto, UpdatePasswordDto } from './dto';
import { UserEntity as User } from './entities/user.entity';
import { PasswordEntity as Password } from './entities/password.entity';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(Password)
    private passwordRepository: Repository<Password>,
  ) {}

  async findAllUsers(): Promise<User[]> {
    return this.userRepository.find();
  }

  async getUser(id: number): Promise<User> {
    const user = await this.userRepository.findOne({ where: { id } });
    if (!user) {
      throw new NotFoundException('User not found');
    }
    return user;
  }

  async createUser(createUserDto: CreateUserDto): Promise<User> {
    await this.validateCreateUserRequest(createUserDto);
    const user = new User();
    Object.assign(user, createUserDto);

    const userPassword = new Password();
    const passwordHash = await bcrypt.hash(createUserDto.password, 10);
    Object.assign(userPassword, { userId: user.id, passwordHash, user });

    await Promise.all([this.userRepository.save(user), this.passwordRepository.save(userPassword)]);
    return user;
  }

  private async validateCreateUserRequest(createUserDto: CreateUserDto): Promise<void> {
    let user: User;
    try {
      user = await this.userRepository.findOne({ where: { login: createUserDto.login } });
      
    } catch (error) {
    }

    if (user) {
      throw new UnprocessableEntityException('User already exists');
    }
  }

  async validateUser(login: string, password: string): Promise<User> {
    const user = await this.userRepository.findOne({ where: { login } });
    if (!user) {
      throw new NotFoundException('User not found');
    }
    const passwordIsValid = await bcrypt.compare(password, user.password.passwordHash);
    if (!passwordIsValid) {
      throw new UnauthorizedException('Credentials are invalid');
    }
    return user;
  }

  async updateUser(id: number, updateUserDto: UpdateUserDto): Promise<User> {
    const user = await this.getUser(id);
    Object.assign(user, updateUserDto);
    return this.userRepository.save(user);
  }


  async updatePassword(currentPassword: string,  updatePasswordDto: UpdatePasswordDto): Promise<Password> {
    const user = await this.getUser(updatePasswordDto.userId);
    if (!user) {
      throw new NotFoundException('User not found');
    }

    if (!(await bcrypt.compare(currentPassword, user.password.passwordHash))) {
      throw new UnauthorizedException('Current password is invalid');
    }

    const password = await this.passwordRepository.findOne({ where: { userId: updatePasswordDto.userId } });
    if (!password) {
      throw new NotFoundException('Password not found');
    }
    
    const passwordHash = await bcrypt.hash(updatePasswordDto.password, 10);
    Object.assign(password, { passwordHash });
    return this.passwordRepository.save(password);
  }

  async resetPassword(userId: number, login: string): Promise<Password> {
    const password = await this.passwordRepository.findOne({ where: { userId, user: { login } } });
    if (!password) {
      throw new NotFoundException('User not found');
    }
    Object.assign(password, { passwordHash: null });
    return this.passwordRepository.save(password);
  }
}