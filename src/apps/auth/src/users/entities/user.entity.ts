import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';

import { PasswordEntity } from './password.entity';

@Entity('user')
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  createdAt: Date;

  @Column()
  updatedAt: Date;

  @Column()
  login: string;

  @Column({ nullable: true })
  email: string;

  @Column()
  isActive: boolean;

  @Column({ nullable: true })
  phoneNumber: string;

  @Column({ nullable: true })
  firstName: string;

  @Column({ nullable: true })
  lastName: string;

  @Column({ nullable: true })
  dateOfBirth: Date;

  @OneToOne(() => PasswordEntity, password => password.user)
  password: PasswordEntity;
}