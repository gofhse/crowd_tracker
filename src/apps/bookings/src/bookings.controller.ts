import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { BookingsService } from './bookings.service';
import { BookingEntity } from './entities/booking.entity';
import { CreateBookingDto } from './dto/create-booking.dto';
import { UpdateBookingDto } from './dto/update-booking.dto';

@Controller('bookings')
export class BookingsController {
  constructor(private readonly bookingsService: BookingsService) {}

  @Get()
  getBookings(): Promise<BookingEntity[]> {
    return this.bookingsService.getAll();
  }

  @Post()
  createBooking(@Body() createBookingDto: CreateBookingDto): Promise<BookingEntity> {
    return this.bookingsService.create(createBookingDto);
  }

  @Get(':bookingId')
  getBooking(@Param('bookingId') bookingId: string): Promise<BookingEntity> {
    return this.bookingsService.getById(+bookingId);
  }

  @Put(':bookingId')
  updateBooking(
    @Param('bookingId') bookingId: string,
    @Body() updateBookingDto: UpdateBookingDto,
  ) {
    return this.bookingsService.update(+bookingId, updateBookingDto);
  }

  @Delete(':bookingId')
  cancelBooking(@Param('bookingId') bookingId: string): Promise<void> {
    return this.bookingsService.delete(+bookingId);
  }
}
