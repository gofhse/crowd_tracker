import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class UpdateBookingDto {
  @IsNumber()
  coworkingId?: number;

  @IsNumber()
  coworkingSpaceId?: number;

  @IsString()
  startTime?: string;

  @IsString()
  endTime?: string;

  @IsNotEmpty()
  @IsNumber()
  userId?: number;
}
