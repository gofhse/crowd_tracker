import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: "booking" })
export class BookingEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'timestamp' })
  created_at: Date;

  @Column({ type: 'timestamp' })
  updated_at: Date;

  @Column({ type: 'integer' })
  coworking_id: number;

  @Column({ type: 'integer' })
  coworking_space_id: number;

  @Column({ type: 'timestamp' })
  start_time: Date;

  @Column({ type: 'timestamp' })
  end_time: Date;

  @Column({ type: 'integer' })
  user_id: number;
}