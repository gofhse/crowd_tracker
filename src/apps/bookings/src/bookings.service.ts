import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BookingEntity } from './entities/booking.entity';
import { Repository } from 'typeorm';
import { CreateBookingDto } from './dto/create-booking.dto';
import { UpdateBookingDto } from './dto/update-booking.dto';

@Injectable()
export class BookingsService {
  constructor(
    @InjectRepository(BookingEntity)
    private readonly bookingRepository: Repository<BookingEntity>,
  ) {}
  
  async getAll(): Promise<BookingEntity[]> {
    return await this.bookingRepository.find();
  }

  async getById(bookingId: number): Promise<BookingEntity> {
    return await this.bookingRepository.findOne({ where: { id: bookingId } });
  }

  // async create(booking: BookingEntity): Promise<BookingEntity> {
  //   return await this.bookingRepository.save(booking);
  // }

  async create(booking: CreateBookingDto): Promise<BookingEntity> {
    const bookingEntity = new BookingEntity();
    const { userId, coworkingId, coworkingSpaceId, startTime, endTime } = booking;
    bookingEntity.coworking_id = coworkingId;
    bookingEntity.coworking_space_id = coworkingSpaceId;
    bookingEntity.user_id = userId;
    bookingEntity.start_time = new Date(startTime);
    bookingEntity.end_time = new Date(endTime);

    return await this.bookingRepository.save(bookingEntity);
  }

  // async update(booking: BookingEntity): Promise<BookingEntity> {
  //   return await this.bookingRepository.save(booking);
  // }

  async update(bookingId: number, booking: UpdateBookingDto): Promise<BookingEntity> {
    const bookingEntity = await this.bookingRepository.findOne({ where: { id: bookingId } });
    if (!bookingEntity) {
      throw new NotFoundException('Booking not found');
    }

    Object.assign(bookingEntity, booking);

    // const { coworkingId, coworkingSpaceId, startTime, endTime } = booking;
    // if (coworkingId) bookingEntity.coworking_id = coworkingId;
    // if (coworkingSpaceId) bookingEntity.coworking_space_id = coworkingSpaceId;
    // if (startTime) bookingEntity.start_time = new Date(startTime);
    // if (endTime) bookingEntity.end_time = new Date(endTime);
    return await this.bookingRepository.save(bookingEntity);
  }

  async delete(bookingId: number): Promise<void> {
    await this.bookingRepository.delete({ id: bookingId });
  }
}
