import { Connection, FilterQuery, Model, SaveOptions, Types, UpdateQuery } from "mongoose";
import { Logger, NotFoundException } from "@nestjs/common";

import { AbstractDocument } from "./abstract.schema";

export abstract class AbstractRepository<TDocument extends AbstractDocument> {
  protected abstract readonly logger: Logger;

  constructor(protected readonly model: Model<TDocument>, protected readonly connection: Connection) {}

  async create(doc: Omit<TDocument, "_id">, options?: SaveOptions): Promise<TDocument> {
    const createdDocument = new this.model({
      ...doc,
      _id: new Types.ObjectId(),
    });
    return (await createdDocument.save(options)).toJSON() as unknown as TDocument;
  }

  async findOne(filterQuery: FilterQuery<TDocument>): Promise<TDocument> {
    const document = await this.model.findOne(filterQuery, {}, { lean: true });
    
    if (!document) {
      this.logger.warn(`Document not found with filterQuery ${JSON.stringify(filterQuery)}`);
      throw new NotFoundException(`Document not found with filterQuery ${JSON.stringify(filterQuery)}`);
    }

    return document;
  }

  async findOneAndUpdate(filterQuery: FilterQuery<TDocument>, update: UpdateQuery<TDocument>): Promise<TDocument> {
    const document = await this.model.findOneAndUpdate(filterQuery, update, { lean: true, new: true });

    if (!document) {
      this.logger.warn(`Document not found with filterQuery ${JSON.stringify(filterQuery)}`);
      throw new NotFoundException(`Document not found with filterQuery ${JSON.stringify(filterQuery)}`);
    }

    return document;
  }

  async upsert(filterQuery: FilterQuery<TDocument>, doc: Partial<TDocument>): Promise<TDocument> {
    return this.model.findOneAndUpdate(filterQuery, doc, { upsert: true, new: true, lean: true });
  }

  async find(filterQuery: FilterQuery<TDocument>): Promise<TDocument[]> {
    return this.model.find(filterQuery, {}, { lean: true });
  }

  async startTransaction() {
    const session = await this.connection.startSession();
    session.startTransaction();
    return session;
  }

  // async deleteOne(filterQuery: FilterQuery<TDocument>): Promise<void> {
  //   await this.model.deleteOne(filterQuery);
  // }

  // async deleteMany(filterQuery: FilterQuery<TDocument>): Promise<void> {
  //   await this.model.deleteMany(filterQuery);
  // }

  // async count(filterQuery: FilterQuery<TDocument>): Promise<number> {
  //   return this.model.countDocuments(filterQuery);
  // }

  // async exists(filterQuery: FilterQuery<TDocument>): Promise<boolean> {
  //   return (await this.count(filterQuery)) > 0;
  // }

  // async updateMany(filterQuery: FilterQuery<TDocument>, update: UpdateQuery<TDocument>): Promise<void> {
  //   await this.model.updateMany(filterQuery, update);
  // }

  // async updateOne(filterQuery: FilterQuery<TDocument>, update: UpdateQuery<TDocument>): Promise<void> {
  //   await this.model.updateOne(filterQuery, update);
  // }

  // async aggregate(pipeline: any[]): Promise<any> {
  //   return this.model.aggregate(pipeline);
  // }

  // async distinct(field: string, filterQuery: FilterQuery<TDocument>): Promise<any> {
  //   return this.model.distinct(field, filterQuery);
  // }

  // async createMany(docs: TDocument[], options?: SaveOptions): Promise<TDocument[]> {
  //   return this.model.insertMany(docs, options);
  // }

  // async createOne(doc: TDocument, options?: SaveOptions): Promise<TDocument> {
  //   return this.model.create(doc, options);
  // }

  // async updateOneById(id: string, update: UpdateQuery<TDocument>): Promise<void> {
  //   await this.model.updateOne({ _id: id }, update);
  // }

  // async updateManyById(id: string, update: UpdateQuery<TDocument>): Promise<void> {
  //   await this.model.updateMany({ _id: id }, update);
  // }

  // async deleteOneById(id: string): Promise<void> {
  //   await this.model.deleteOne({ _id: id });
  // }

  // async deleteManyById(ids: string[]): Promise<void> {
  //   await this.model.deleteMany({ _id: { $in: ids } });
  // }
}